# todo-pwa-litelement-ts

A open layer test app as PWA. Written with TypeScript and Lit-Element.

# Accessible under

https://ladida.gitlab.io/openlayer-pwa-litelement-ts

# Installation

```sh
npm i
```

# Run

dev:
```sh
npm run dev
```

dev with service worker enabled:
```sh
npm run dev:sw
```

Prod:
```sh
npm run prod
cd dist
serve .
```