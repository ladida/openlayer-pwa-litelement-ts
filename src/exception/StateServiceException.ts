export class StateServiceException {

  private msg: string;

  constructor(msg: string) {
    this.msg = msg;
  }

  public getMessage(): string {
    return this.msg;
  }
}