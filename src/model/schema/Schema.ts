import { DBSchema } from 'idb';

export class SchemaNames {

  public static DB_NAME: string = '';

  public static EMPTY: any = 'empty';
}

/**
 * Schema für Indexeddb (idb)
 */
export interface Schema extends DBSchema {
  'empty': {
    key: string;
    value: {
      id: string,
    };
  };
}