import { StateService } from "./StateService";
import { StateServiceException } from "../../../exception/StateServiceException";

/**
 * State service for the installation of a new service worker.
 */
export class ServiceWorkerInstallationStateService extends StateService {

  private state: string;

  /**
   * the new service worker
   */
  private serviceWorker: ServiceWorker;

  public async init() {
  }

  /**
   * should called when a new service worker is installing
   */
  public installingHandler(serviceWorker: ServiceWorker) {
    this.state = ServiceWorkerInstallationStateType.INSTALLING;

    this.serviceWorker = serviceWorker;
  }

  /**
   * should called when the new service worker is installed
   */
  public installedHandler() {
    this.checkIfSwExists();
    this.state = ServiceWorkerInstallationStateType.INSTALLED;

    this.informObservers();
  }

  /**
   * should called when the new service worker is activating
   */
  public activatingHandler() {
    this.checkIfSwExists();
    this.state = ServiceWorkerInstallationStateType.ACTIVATING;

    this.informObservers();
  }

  /**
   * should called when the new service worker is activated
   */
  public activatedHandler() {
    this.checkIfSwExists();
    this.state = ServiceWorkerInstallationStateType.ACTIVATED;

    this.reload();
  }

  /**
   * should called when the "statechange" event has a unknown state
   */
  public unknownStateHandler() {
    console.error('[ServiceWorkerInstallationStateService.js] statechange ' + this.serviceWorker.state);
  }

  /**
   * starts the activation of the new serviceworker
   */
  public activate() {
    this.checkIfSwExists();
    this.serviceWorker.postMessage('SKIP_WAITING');
  }

  /**
   * reload the page, so the new worker can take control
   */
  public reload() {
    this.checkIfSwExists();
    this.serviceWorker.postMessage('CLAIM');
    location.reload();
  }


  public getState(): string {
    return this.state;
  }

  private checkIfSwExists() {
    if (this.serviceWorker === undefined) {
      throw new StateServiceException("ServiceWorker is undefined: call installing(newWorker) first.");
    }
  }
}

/**
 * the states for the service worker installtion
 */
export class ServiceWorkerInstallationStateType {
  public static INSTALLING: string = 'INSTALLING';
  public static INSTALLED: string = 'INSTALLED';
  public static ACTIVATING: string = 'ACTIVATING';
  public static ACTIVATED: string = 'ACTIVATED';
}


/**
 * Factory for the ServiceWorkerInstalltionStateService.
 */
export class ServiceWorkerInstallationStateServiceFactory {
  private static instance: ServiceWorkerInstallationStateService;

  public static async getInstance(): Promise<ServiceWorkerInstallationStateService> {
    if (!ServiceWorkerInstallationStateServiceFactory.instance) {
      ServiceWorkerInstallationStateServiceFactory.instance = new ServiceWorkerInstallationStateService();
      await ServiceWorkerInstallationStateServiceFactory.instance.init();
    }
    return ServiceWorkerInstallationStateServiceFactory.instance;
  }
}