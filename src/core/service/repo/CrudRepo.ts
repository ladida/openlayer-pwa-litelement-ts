import { BaseRepo } from './BaseRepo';
import { Entity } from '../../model/Entity';

/**
 * CRUD Repo.
 */
export abstract class CrudRepo<T extends Entity> extends BaseRepo<T> {

  public TAB_NAME: any;

  /**
   * Retrieve all entities.
   */
  public async findAll(): Promise<T[]> {
    var dataFromDb = await this.db.getAll(this.TAB_NAME);
    var entities: T[] = [];

    for (let data of dataFromDb) {
      entities.push(this.toEntity(data));
    }

    return entities;
  }

  /**
   * Retrieve a entity by id.
   */
  public async findById(id: string): Promise<T> {
    var dataFromDb = await this.db.get(this.TAB_NAME, id);
    var entity: T = this.toEntity(dataFromDb);
    return entity;
  }

  /**
   * Create an entity.
   * 
   * @param entity entity to create
   */
  public async create(entity: T) {
    try {
      await this.db.put(
        this.TAB_NAME,
        this.fromEntity(entity),
        entity.id);
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Update an entity.
   * 
   * @param entity entity the is updated
   */
  public async update(entity: T) {
    entity.update();
    await this.create(entity);
  }

  /**
   * Delete the given entity.
   * 
   * @param entity entity to delete
   */
  public async delete(entity: T) {
    try {
      await this.db.delete(
        this.TAB_NAME,
        entity.id);
    } catch (e) {
      console.error(e);
    }
  }
}