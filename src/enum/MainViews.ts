/**
 * Represent the navigation targets for the main view
 */
export class MainViews {
  public static MAP_ONLINE: string = 'MAP_ONLINE';
  public static MAP_OFFLINE: string = 'MAP_OFFLINE';
  public static ABOUT: string = 'ABOUT';
}
