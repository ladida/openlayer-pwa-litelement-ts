import { customElement } from 'lit-element';
import { ButtonComp } from './button-comp';

@customElement('blue-button-comp')
export class BlueButtonComp extends ButtonComp {

  constructor() {
    super();

    this.bgColor = '#2196F3';
    this.textColor = 'white';
  }
}