import { customElement } from 'lit-element';
import { ButtonComp } from './button-comp';

@customElement('white-button-comp')
export class WhiteButtonComp extends ButtonComp {

  constructor() {
    super();

    this.bgColor = 'white';
    this.textColor = '#616161';
  }
}