import { html, css, customElement, property } from 'lit-element';
import { BaseComp } from '../../../core/gui/BaseComp';

@customElement('checkbox-comp')
export class CheckboxComp extends BaseComp {

  @property({ type: Boolean })
  public checked = false;

  private changeEvent: Event;

  constructor() {
    super();
    this.changeEvent = new Event('change');
  }

  render() {
    return html`
      <input class="check" type="checkbox" .checked=${this.checked} @change="${this.change}"/>
    `;
  }

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .check {
        width: 20px;
        height: 20px;
        position: relative;
      }
    `;
  }

  private change(e: { target: HTMLInputElement }) {
    this.checked = e.target.checked;
    this.dispatchEvent(this.changeEvent);
  }
}