import { html, css, customElement } from 'lit-element';
import { BaseComp } from '../../core/gui/BaseComp';

import { Map, View } from 'ol';
import { default as TileLayer } from 'ol/layer/Tile';
import { ScaleLine } from 'ol/control';
import { OSM, XYZ, TileDebug } from 'ol/source';
import { fromLonLat } from 'ol/proj.js';
import { MouseWheelZoom, DoubleClickZoom, KeyboardZoom } from 'ol/interaction';
import { WithoutShadowDomBaseComp } from '../../core/gui/WithoutShadowDomBaseComp';

@customElement('map-offline-view')
class MapOfflineView extends BaseComp {

  render() {
    return html`
      <link rel="stylesheet" href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v6.2.1/css/ol.css">
      <div id="map" style="height: 100%; width: 100%;"></div>
    `;
  }

  static get styles() {
    return css`
      :host {
        display: block;
        height: 100%;
        width: 100%;
      }
    `;
  }

  firstUpdated() {
    const map = new Map({
      controls: [
        new ScaleLine()
      ],
      layers: [
        new TileLayer({
          source: new XYZ({
            url: 'https://ladida.gitlab.io/openlayer-pwa-litelement-ts/img/map/{z}-{x}-{y}.png'
          })
        })
      ],
      view: new View({
        center: fromLonLat([14.236764, 50.908592]),
        zoom: 17,
        minZoom: 14,
        maxZoom: 17
      }),
    });

    var div = this.getElementById("map");
    if (div) {
      map.setTarget(div);
    }
  }
}