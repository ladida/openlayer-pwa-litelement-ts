import { html, css, customElement, property } from 'lit-element';
import { MainViews } from '../../enum/MainViews';
import { NavigationStateService, NavigationStateServiceFactory } from '../../core/service/state/NavigationStateService';
import { IStateObserver } from '../../core/service/state/IStateObserver';
import { BaseComp } from '../../core/gui/BaseComp';

/**
 * Main view. This is the container for all the different view of the app.
 */
@customElement('main-view')
class MainView extends BaseComp implements IStateObserver {

  @property()
  private view: MainViews;

  private navigationStateService: NavigationStateService;

  constructor() {
    super();
  }

  protected async initialize() {
    super.initialize();
    this.navigationStateService = await NavigationStateServiceFactory.getInstance();
    this.navigationStateService.registerObserver(this);
    this.navigationStateService.openMapOfflineView();
  }

  async stateChanged() {
    this.view = await this.navigationStateService.getActualView();
  }

  render() {
    return html`
      <div class="app-container">
        <header-view class="header"></header-view>

        <div class="content">
          ${this.view == MainViews.MAP_OFFLINE ? html`<map-offline-view></map-offline-view>` : html``}
          ${this.view == MainViews.MAP_ONLINE ? html`<map-view></map-view>` : html``}
          ${this.view == MainViews.ABOUT ? html`<about-view></about-view>` : html``}
        </div>
      </div>
    `;
  }

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .app-container {
        width: 100vw;
        height: 100vh;
        display: flex;
        flex-direction: column;
        flex-wrap: nowrap;
      }
      .header {
        flex-shrink: 0;
      }
      .content {
        width: 100%;
        flex-grow: 1;
        overflow: auto;
      }
    `;
  }
}